/*****************************************
<pb24_driver.h>
Headerfile mit Spincore spezifischen Daten 
******************************************/
#ifndef _pb24_driver_H_
  #define _pb24_driver_H_

#define PB24 

int detect_boards();
int select_board(int numBoards);
int input_clock(double *retClock);

// Eigene, und/oder angepasste Funktionen

int pbInitialize(void);
int pbLoadList(ListType list); 

#endif /* _pb24_driver_H_ */
