/*  HASU: Module pb24_driver.c von Bsp. Code SpinCore destilliert
 *  fuer externe Verwendung und ev. leichtere Anpassung
 */
 
/**   
 * \file pb24_ex1.c
 * PulseBlaster24 Example 4
 *
 * \brief This program demonstrates use of the WAIT op code.
 * This program will generate ten pulses 100ms long (50% duty cycle) on all TTL
 * outputs and then wait for the hardware trigger. Upon triggering, the board
 * will  wait 100ms then repeat the pulse pattern and wait for another trigger.
 * This behavior will repeat until pb_stop is called.
 * \ingroup pb24
 */
/* Copyright (c) 2009 SpinCore Technologies, Inc.
 *   http://www.spincore.com
 */
#include <formatio.h>
#include <stdio.h>
#include <stdlib.h>
#include "toolbox.h"  // wegen der Liste als Uebergabeparameter notwendig

#include "spinapi.h"

//es geht nicht ohne include der globalen Daten
#include "pulsmaske.h"
#include "pm_global.h"
#include "error.h"



void pause(void)
{
  printf("Press enter to continue...");
  char buffer = getchar();
  while(buffer != '\n' && buffer != EOF);
  getchar();
}

// Bereitet Board vor bis und mit MasterClock Eingabe
int pbInitialize(void)
{

  int numBoards;
//  double clock; //nur gebraucht, wenn Master CLK einstellbar ist

  //Uncommenting the line below will generate a debug log in your current
  //directory that can help debug any problems that you may be experiencing
#ifdef TESTEN
  pb_set_debug(1);
#endif

//  printf("Copyright (c) 2010 SpinCore Technologies, Inc.\n\n");

//  printf("Using SpinAPI library version %s\n", pb_get_version());

  /*If there is more than one board in the system, have the user specify. */
  if ((numBoards = detect_boards()) > 1) {
    select_board(numBoards);
  }

  if (pb_init() != 0) {
    printf("Error initializing board: %s\n", pb_get_error());
    pause();
    return -1;
  }
/*  
  //User input clock
  input_clock(&clock);

  printf("Clock frequency: %lfMHz\n\n", clock);
  printf("All outputs should turn on and then off 3 times with a 100us "
         "period and 50%% duty cycle. The board will then wait for a "
       "hardware trigger signal before repeating.\n");
*/
  // Tell the driver what clock frequency the board has (in MHz)
  pb_core_clock(MASTERCLK);
  
  return 0;
}


int pbLoadList(ListType list)
{  
  int start, status, itemCount=0;
  sChgpatt *tempItemN = NULL;  //aktuelles Element bei n
  sChgpatt *tempItemNm1 = NULL;//vorgaengiges Element bei n-1
  char buf[MAXBUF]="";
  
  // Bereitet Board vor bis und mit MasterClock Eingabe
  if(pbInitialize() < 0)
    return ERR_PULSGENI_INIT;
  
  pb_start_programming(PULSE_PROGRAM);
  
  // Einfuegen CONTINOUS fuer Triggered Start 
  // -> 70ns Instruktionsdauer
  pb_inst(0x000000, CONTINUE, 0, 70.0);
  
  // Instruction  - Wait for a hardware trigger.
  // Flags = 0x000000, OPCODE = WAIT
  // Gibts eine Mindestdauer? fuer die 'exit latency'
  start = pb_inst(0x0, WAIT, 0, 1.0 * us);
  
  // Liste zu Instruktionen verarbeiten mit folgendem Format:
  // Flags:von n-1; CONT; 0 (opcode); DelayCount: Index (n) - Index (n-1)
  if ((itemCount = ListNumItems(list)) > 0)
  {
    //Erstes Elemet der Liste, Listenzaehler starten mit 1
    if ((tempItemN = ListGetPtrToItem (list, 1))!= NULL){
      ; //tue nichts
    }else return ERR_LIST_PREPARE;
    //Start the index at 2 to zweitletztem Element
    for (int n = 2; n < itemCount; n++)
    {
      tempItemNm1 = tempItemN; //????  geht das  ????
      //Listenelement lesen
      if ((tempItemN = ListGetPtrToItem (list, n))!= NULL)
      {
        pb_inst(tempItemNm1->flags, CONTINUE, 0,
                (double)(tempItemN->index - tempItemNm1->index) * us / MULT_FOR_P);
      }else return ERR_LIST_PREPARE;//End if
    }//End for
    //Letztes Listenelement
    tempItemNm1 = tempItemN;
    if ((tempItemN = ListGetPtrToItem (list, END_OF_LIST))!= NULL){
      pb_inst(tempItemNm1->flags, CONTINUE, 0,
                (double)(tempItemN->index - tempItemNm1->index) * us / MULT_FOR_P);
      //letztes Flag muesste immer 0 sein - dann Triggertotzeit hinzufügen
      pb_inst(tempItemN->flags, CONTINUE, 0, 50*us);
    }else return ERR_LIST_PREPARE;
    
  }else return NULL;//End if
  
  // Instruction - Branch back to the beginning of the program.
  // Flags = 0x000000, OPCODE = BRANCH, 1us zu Trigger-Totzeit addiert
  pb_inst(0x0, BRANCH, start, 1.0 * us);

  pb_stop_programming();

  // Trigger the pulse program
  pb_reset();
  pb_start();

  //Read the status register
  status = pb_read_status();
  // Kontrolltext auf GUI geschrieben
  Fmt(buf,"%s<Status des PB24: %i",status);
  writeConsole(buf);

  pb_stop();
  pb_close();

  return 0;
}

int detect_boards()
{
  int numBoards;

  numBoards = pb_count_boards();  /*Count the number of boards */

  if (numBoards <= 0) {
    printf
        ("No Boards were detected in your system.\n\n");
    pause();
    exit(-1);
  }

  return numBoards;
}

int select_board(int numBoards)
{
  int choice;

  do {
    printf
        ("Found %d boards in your system. Which board should be used? "
         "(0-%d): ", numBoards, numBoards - 1);
    fflush(stdin);
    scanf("%d", &choice);

    if (choice < 0 || choice >= numBoards) {
      printf("Invalid Board Number (%d).\n", choice);
    }
  } while (choice < 0 || choice >= numBoards);

  pb_select_board(choice);
  printf("Board %d selected.\n", choice);

  return choice;
}

int input_clock(double *retClock)
{ 
  char clock[256];
  double user_input = -1.0;
  char* user_end; 
    
  do {
    fflush(stdin);
    printf("\nPlease enter internal clock frequency (MHz): ");
    scanf("%256s", clock);
    user_input = strtod(clock,&user_end);
  } while(user_end[0] != '\0');

    *retClock = user_input;
  return 0;
}
