/****************************************************/
/*  Projekt:  G5100 Pattern Generator         */
/*  Datei:    Translation_Text.h            */
/*  Datum:    15.11.2016                */
/*  Ersteller:  Gabriel K�ppeli             */
/*  Funktion:   In dieser Datei sind die �ber-    */
/*        setzungen der GUI aufgef�hrt.   */
/****************************************************/

// P:\elektronik\_Lehrlinge\Daten_GabrielKaeppeli_15-17\Projekte\G5100_AWG_Picotest\Software\VISA_UI_direct

#define EN        1
#define DE        2
#define LANGUAGE    DE        // Hilfstext-Sprache 

#ifdef LANGUAGE             // Beschreibung 
  #if (LANGUAGE == DE  )        // Deutsch
    #define HELP_T          "Hilfe"
    #define HELP_TEXT       "Die Daten k�nnen hier eingegeben werden. Es stehen insgesamt 16 Kan�le zur Verf�gung.\n\
      Um einen Kanal zu aktivieren, muss das erste K�stchen ausgew�hlt werden.\n\
      - In das erste Textfeld kann ein eigener Name eingegeben werden.\n\
      - Bei dem �Trigger�-Feld w�hlt man den aktiven Kanal, der als Startbezug dienen soll.\n\
        W�hlt man T0, f�ngt der Puls bei der positiven Flanke des externen HW Triggers an.\n\
      -  �Start (Delay)� entspricht der Verz�gerung in Mikrosekunden, die auch negativ\n\
        sein kann bei der Wahl des ensprechenden Vorzeichens.    \n\
        Pulsbeginn = Trigger + StartDelay         \n\
      - Die Pulsdauer wird im �Pulsdauer�-Feld in Mikrosekunden eingegeben. Diese Zahlen werden     \n\
        auf zwei Nachkommastellen gerundet.   \n\
      - Die Taktfrequenz ist in dieser Version fix 100MHz, es resultiert ein 10ns Jitter.\n\
      ***********************************************************************************\n\
      �Create&Save� erzeugt auch ein tempor�res File 'timelist.dat', welches sich laden l�sst."
    #define FINISH_T        "Fertig!" 
    #define ERROR_T         "Fehler!"
    #define WELCOME_T       "Willkommen!"
    #define NO_DEVICE_FOUND_T   "Kein Ger�t gefunden. Bitte Ger�t anschalten und anschliessen."
    #define NO_WAVEFORM_CREATED_T "Es wurde kein Muster erstellt. Bitte erstellen sie ein Muster neu oder �ffnen sie eine Datei." 
    #define NO_PATT_NAME_T      "Bitte einen Namen eingeben."
    #define NO_OVERWRITE_T      "Daten wurden nicht �berschrieben. Bitte den Namen �ndern oder das �berschreiben zulassen."
    #define SUCCESS_WRITE_DEVICE_T  "Daten wurden erfolgreich �bermittelt."

  #elif (LANGUAGE == EN )       // Englisch
    #define HELP_T          "Help"
    #define HELP_TEXT       "The data can be entered here. There are 16 available channels. To activate one, you have   \n\
      to check the box in the first column. It is also possible to activate all channels at the   \n\
      same time with the checkbox at the bottom. In the first Textbox you can enter an own name.  \n\
      In the �Trigger� field, the trigger can be chosen. If you select �T0�, the pulse channel  \n\
      starts on the positive edge from the extern trigger. Otherwise it starts, when the chosen   \n\
      starts. This channel hasn�t to be active. It is possible to enter a delay in the �Start   \n\
      (Delay)� field, so that the pulse only starts after his trigger and the delay time.     \n\
      The delay time can be positive and negative. The Pulselength is entered in the        \n\
      �Pulselength�-field in microseconds. The numbers are rounded to two decimal places. The last\n\
      column is to invert the pulse. The frequency in this version is fixed to 50 MHz, so there is\n\
      a jitter of 20 ns. If you click �Help�, you see this text."
    #define FINISH_T        "Finished!" 
    #define ERROR_T         "Error!"
    #define WELCOME_T       "Welcome!"
    #define NO_DEVICE_FOUND_T   "No device found. Please turn on and connect." 
    #define NO_WAVEFORM_CREATED_T "There is no pattern. Please create one or open from a file." 
    #define NO_PATT_NAME_T      "Please insert a name."
    #define NO_OVERWRITE_T      "Data not overwritten. Please change the name or allow overwriting."
    #define SUCCESS_WRITE_DEVICE_T  "Data was successful transmitted to the device."
  #else               // Andere Sprache
    #define HELP_TEXT       "Helptext not written in asked language yet."
  #endif
#endif

