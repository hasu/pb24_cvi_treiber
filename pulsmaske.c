/*****************************************************************************
 *  "pulsmaske.c" Modul (File), welches das 'main()' enthaelt;
 *  hasu_200407: Beginn  
 *  Programm zeigt eine Eingabemaske in Tabellenform.
 *  Die Eingabe wird in Befehle an einen SpinCore PulsBlaster24-USB umgesetzt. 
 *  Pro Kanal kann ein positiver Puls erzeugt werden.
 *  hasu_200422
 *  
 *
 ****************************************************************************/
#include <formatio.h>
#include <ansi_c.h>
#include <cvirte.h>   
#include <userint.h>


#include "toolbox.h"    //CVI Bibliothek mit nuetzlichen Fkt
#include "pulsmaske.h"  //Header des UIR Files
#include "pm_global.h"  //Header mit globalen Daten und Definitionen
#include "Error.h"            //Von GAKA uebernommen, vorerst ->hasu200408
#include "Translation_Text.h" //Von GAKA uebernommen, vorerst ->hasu200408
#include "pb24_driver.h" //PulsGenerator spezifischer Header

#define TESTEN 1


int main (int argc, char *argv[])
{
  
  FILE* fileHandle;
  int status=0;
  
  if (InitCVIRTE (0, argv, 0) == 0)
    return -1;  /* out of memory */
  if ((ph = LoadPanel (0, "pulsmaske.uir", PANEL)) < 0)
    return -1;
  
  DisplayPanel (ph);
  //falls ein Temporaeres File vorhanden
  if(FileExists(FILE_FOR_GLOBAL_VAR,0) == TRUE){
    fileHandle = fopen(FILE_FOR_GLOBAL_VAR, "r");
    if(fileHandle == NULL){
      //Fehler unkritisch, der Operator muss Tabelle von Hand ausfuellen
      SetCtrlVal (ph, PANEL_NUMERIC_ERROR, ERR_NO_FILE_SELECTED);   
      MessagePopup (ERROR_T, "Achtung!! File konnte nicht gelesen werden\n\
                    Fuellen Sie Maske von Hand aus");             
    }else{
       //liest Daten aus Tempfile in die globale Variable
       //die Index sind noch nicht erneuert
       status = readFileToGlobalVar(fileHandle);
       fclose(fileHandle);
       if(status != 0){
         //Fehler unkritisch, der Operator muss Tabelle von Hand ausfuellen
         SetCtrlVal (ph, PANEL_NUMERIC_ERROR, ERR_FILE);   
         MessagePopup (ERROR_T, "Achtung!! File konnte nicht gelesen werden\n\
                       Fuellen Sie Maske von Hand aus");
       }
    }
  }else MessagePopup("InitInfo", "Kein temporaeres File im Verzeichnis");
  
  RunUserInterface ();
  DiscardPanel (ph);
  
  CloseCVIRTE();
  
  return 0;
}

//toter Code, hasu200417
int CVICALLBACK calcCycle (int ph, int control, int event,
    void *callbackData, int eventData1, int eventData2)
{
  switch (event)
  {
    case EVENT_COMMIT:

      break;
  }
  return 0;
}

//Aufruf durch 'Create and Save' Control auf UIR
int CVICALLBACK makeFile (int ph, int control, int event,
    void *callbackData, int eventData1, int eventData2)
{
  int status;
  char string[MAXBUF];
  FILE *fileHandle;
  
  switch (event)
  {
    case EVENT_COMMIT:
      // Daten der UI in ein struct f�llen
      status = fillData();                    
      if(status != SUCCESS) 
      { 
        // Fehler unkritisch
        Fmt(string,"%s<Some time input was reset %i",status);
        writeConsole(string);
        break;                            
      }
      //bestimmt die absoluten Startwerte bezueglich T0
      status = detAbsStart();                 
      if(status != SUCCESS)                                 
      {                               
        //Fehler kritisch, der Operator muss Tabelle neu schreiben
        SetCtrlVal (ph, PANEL_NUMERIC_ERROR, status);   
        sprintf(string, "Achtung!! Tabelle kontrollieren\nErrorcode: %i", status);
        MessagePopup (ERROR_T, string);             
        break;                            
      }
      // Daten der Pulskanaele werden in eine Flankenliste eingetragen 
      status = createList();                   
      if(status != SUCCESS)
      {                               
        SetCtrlVal (ph, PANEL_NUMERIC_ERROR, status);   
        sprintf(string, "Errorcode: %i", status);
        MessagePopup (ERROR_T, string);
        break;        
      }
      SetCtrlVal (ph, PANEL_NUMERIC_ERROR, NO_ERROR); // Kein_Fehler(0) in  UI schreiben
      writeConsole("Liste erfolgreich kreiert");
      
      //loescht altes TemporaerFile falls vorhanden
      RemoveFileIfExists(FILE_FOR_GLOBAL_VAR);
      fileHandle = fopen(FILE_FOR_GLOBAL_VAR, "w");
      if(fileHandle == NULL){
        //Fehler kritisch, der Operator muss Tabelle neu speichern
        SetCtrlVal (ph, PANEL_NUMERIC_ERROR, ERR_FILEPATH_NOT_FOUND);   
        sprintf(string, "Achtung!! File konnte nicht geschrieben werden\nErrorcode: %i", ERR_FILEPATH_NOT_FOUND);
        MessagePopup (ERROR_T, string);
        //dimmt den Programmierknopf
        SetCtrlAttribute(ph, PANEL_SEND, ATTR_DIMMED, EIN);
        break;
      }
      //schreibt die globale Variable in TemporaerFile 
      status = saveGlobalVar(fileHandle);                 
      if(status != SUCCESS)                                 
      {                               
        //Fehler kritisch, der Operator muss Tabelle neu schreiben
        SetCtrlVal (ph, PANEL_NUMERIC_ERROR, status);   
        sprintf(string, "Achtung!! File konnte nicht geschrieben werden\nErrorcode: %i", status);
        MessagePopup (ERROR_T, string);
        //dimmt den Programmierknopf
        SetCtrlAttribute(ph, PANEL_SEND, ATTR_DIMMED, EIN);
        break;                            
      }
      fclose(fileHandle);
      //ermoeglicht den Programmierknopf
      SetCtrlAttribute(ph, PANEL_SEND, ATTR_DIMMED, AUS);
  }//End switch
  return 0;
}

int CVICALLBACK help (int ph, int control, int event,
    void *callbackData, int eventData1, int eventData2)
{
  switch (event)
  {
    case EVENT_COMMIT:
      MessagePopup("Help", HELP_TEXT);
      break;
  }
  return 0;
}

int CVICALLBACK openFile (int ph, int control, int event,
    void *callbackData, int eventData1, int eventData2)
{
//  FILE *fileHandle;
//  int status=0;
  
  switch (event)
  {
    case EVENT_COMMIT:
      
        MessagePopup ("Open File", "Noch nicht implementiert");
/*      if(FileExists(FILE_FOR_GLOBAL_VAR,0) == TRUE){
        fileHandle = fopen(FILE_FOR_GLOBAL_VAR, "r");
        if(fileHandle == NULL){
          //Fehler unkritisch, der Operator muss Tabelle von Hand ausfuellen
          SetCtrlVal (ph, PANEL_NUMERIC_ERROR, ERR_NO_FILE_SELECTED);   
          MessagePopup (ERROR_T, "Achtung!! File konnte nicht gelesen werden\n\
                        Fuellen Sie Maske von Hand aus");             
          break;
        }
        //liest Daten aus Tempfile in die globale Variable
        //die Index sind noch nicht erneuert
        status = readFileToGlobalVar(fileHandle);
        if(status != 0){
          //Fehler unkritisch, der Operator muss Tabelle von Hand ausfuellen
          SetCtrlVal (ph, PANEL_NUMERIC_ERROR, ERR_FILE);   
          MessagePopup (ERROR_T, "Achtung!! File konnte nicht gelesen werden\n\
                        Fuellen Sie Maske von Hand aus");
        }
      }
      break; */
  }//End switch
  return 0;
}

int CVICALLBACK QuitCallback (int ph, int control, int event,
    void *callbackData, int eventData1, int eventData2)
{
  switch (event)
  {
    case EVENT_COMMIT:
      QuitUserInterface (0);
      break;
  }
  return 0;
}

//Erzeugt die bereinigte Flankenliste und
//laedt sie als Instruktionen auf den PulsGeni
//Aufruf durch 'Program device' Control auf UIR
int CVICALLBACK progGeni (int ph, int control, int event,
    void *callbackData, int eventData1, int eventData2)
{
  ListType tempList; //wird die abzuarbeitende Liste f�r die Flanken enthalten
  int status;
  char string[MAXBUF];
  
  switch (event)
  {
    case EVENT_COMMIT:
      tempList = elimEqualIndex(lFlanken);
#ifdef TESTEN      
      //Schreibt Liste auf stdio zur Kontrolle
      listPrint(tempList);
#endif
      status = pbLoadList(tempList);
      if(status != NULL){
        //dimmt den Programmierknopf
        SetCtrlAttribute(ph, PANEL_SEND, ATTR_DIMMED, EIN);
        //Fehler kritisch, der Operator muss Hardware kontrollieren
        SetCtrlVal (ph, PANEL_NUMERIC_ERROR, status);   
        sprintf(string, "Achtung!! Generator konnte nicht beschrieben werden\nErrorcode: %i", status);
        MessagePopup (ERROR_T, string);             
        break;
      }
      break;
  }
  return 0;
}

//Steuert die Kanaleingabe ueber das Flag Control (Active)
//Ist bei jedem der Kanalflags 1 bis MAXCHAN hinterlegt
int CVICALLBACK setChanProp (int ph, int control, int event,
    void *callbackData, int eventData1, int eventData2)
{
  char buf[MAXBUF];
  int i,j, flag;
  
  switch (event)
  {
    case EVENT_COMMIT:
      // liest den Wert des ActiveFlags
      GetCtrlVal(ph, control, &flag);
      // loopt durch die Kanaele, Start bei Ch1 = bit1
      for(i=1; i<MAXCHAN; ++i){
        //wenn der ausl�sende Kanal gefunden ist
        if(control == ch[i][0]){
          //wird geprueft, ob er ein- oder ausgeschaltet wurde
          switch (flag){
            case EIN:
              //alle Kanalparameter werden aktiviert ausser FLAG
              for(j=1; j<7; ++j){
                SetCtrlAttribute(ph, ch[i][j], ATTR_DIMMED, AUS);
              }
        //Kanalparameter werden in globale Variable geschrieben
              channel[i].act = EIN;
              GetCtrlVal(ph, ch[i][1], channel[i].name);
              GetCtrlVal(ph, ch[i][2], &channel[i].trig);
              GetCtrlVal(ph, ch[i][3], &channel[i].sign);
              GetCtrlVal(ph, ch[i][4], &channel[i].delay);
              GetCtrlVal(ph, ch[i][5], &channel[i].lenght);
              GetCtrlVal(ph, ch[i][6], &channel[i].logic);
        // Kontrolltext auf GUI geschrieben
              Fmt(buf,"%s<Turn on Channel %i",i);
              writeConsole(buf);
              break;
            case AUS:
              //alle Kanalparameter werden deaktiviert durch dimmen 
              //ausser FLAG, in globaler Variabel ausgeschaltet
              channel[i].act = AUS;
              for(j=1; j<7; ++j){
                SetCtrlAttribute(ph, ch[i][j], ATTR_DIMMED, EIN);
              }
              // Kontrolltext auf GUI geschrieben
              Fmt(buf,"%s<Turn off Channel %i",i);
              writeConsole(buf);
              break;
            default:
              writeConsole("Channel select error");
          }// Ende switch    
        }// Ende if
      }// Ende for
      //dimmt den Programmierknopf
      SetCtrlAttribute(ph, PANEL_SEND, ATTR_DIMMED, EIN);  
      break;
  }// Ende switch
  
  
  return 0;
}


/*************************** Berechnungsfunktionen  **************************/ 

// fillData: f�llt das timing struct mit den Werten der Benutzeroberfl�che
// f�r jeden Kanal und macht rudimentaere Tests
// Rueckgabe: Fehlernummer
// Sideeffekt: beschreibt globale Variable
int fillData(void)
{
  int i, err=0;
  long long lival; //
  double mclk; //Master CLK von UIR, momentan konstant

  GetCtrlVal (ph, PANEL_CLOCK, &mclk);
  //behandelt T0
  channel[TNULL].act = EIN;
  channel[TNULL].sign = 1;
  strcpy(channel[TNULL].name,"T0");
  //loopt durch die GUI 'Tabelle'
  for(i=1; i<MAXCHAN; ++i){
    GetCtrlVal(ph, ch[i][1], channel[i].name);
    GetCtrlVal(ph, ch[i][5], &channel[i].lenght);
    //wenn ein aktiver Kanal gefunden ist
    if(channel[i].act == EIN){
      //wird die Zeile (Timing der Kanale) eingelesen
      GetCtrlVal(ph, ch[i][2], &channel[i].trig);    
      GetCtrlVal(ph, ch[i][3], &channel[i].sign);
      //Granularitaet berichtigt - Overflow testen & Default setzen
      GetCtrlVal(ph, ch[i][4], &channel[i].delay);
      lival= (long long)(channel[i].delay * MULT_FOR_P / PER_RES);
      if(lival > MAXTIME_P){
        channel[i].delay = 0.0;
        SetCtrlVal(ph, ch[i][4], 0.0);
        err = ERR_TIMING;
      }
      //Granularitaet berichtigt - Overflow testen & Default setzen
      GetCtrlVal(ph, ch[i][5], &channel[i].lenght);
      //64bit int fuer Variable um 2^32-1 abzudecken
      lival= (long long)(channel[i].lenght * MULT_FOR_P / PER_RES);
      if(lival > MAXTIME_P || lival < MINDELAY_P){
        channel[i].lenght = 1.0;
        SetCtrlVal(ph, ch[i][5], 1.0);
        err =  ERR_TIMING;
      }
    
      GetCtrlVal(ph, ch[i][6], &channel[i].logic);
    
    }// Ende if
  }// Ende for
  
  return err;
}

// detAbsStart(): bestimmt den absoluten Startwert bezueglich T0
// f�r jeden Kanal, behandelt Zirkelbezug und macht rudimentaere Tests
//Teile davon aus GAKA G5100 Programm
int detAbsStart(void)
{
  int i, status, err=0;
  long long lival; 
  double start;          //absoluter Start in us
  
  for(i=1; i<MAXCHAN; ++i){
    //wenn ein aktiver Kanal gefunden ist
    if(channel[i].act == EIN){
      status =0;
      //Rekursive Fkt aufrufen zur Rueckverfolgung Start  bis T0
      start = startDelay(i, &status);
      //Wenn ein Zirkelbezug vorliegt, Fehler melden
      if(status > MAXCHAN)
        return ERR_CIRCULAR_REFERENCE + i;
      // Wenn der Zeitpunkt negativ ist, Fehler melden
      if(start <0)
        return ERR_MINIMAL_TIME + i;
      //soweit alles gut
      lival = (long long)(start * MULT_FOR_P / PER_RES);
      if(lival > MAXTIME_P){
        channel[i].delay = 1.0;
        SetCtrlVal(ph, ch[i][4], 0.0);
        err =  ERR_TIMING;
      }else{
        channel[i].start = lival;
      }
      lival= (long long)((start + channel[i].lenght) * MULT_FOR_P / PER_RES);
      if(lival > MAXTIME_P || lival < MINDELAY_P){
        channel[i].lenght = 1.0;
        SetCtrlVal(ph, ch[i][5], 1.0);
        err =  ERR_TIMING;
      }else{
        channel[i].stop = lival;
      }
      
    }// Ende if
  }// Ende for
  
  return err;
}

// startDelay: ermittelt den Startzeitpunkt des Pulses
// Wenn der Trigger nicht T0  ist, oeffnet sich das Unterprogramm erneut und 
// ermittelt den Startzeitpunkt des Triggers.
// Fehlerueberpruefung mit check: falls das Programm haeufiger
// wie 16 aufgerufen wird hat es einen Zirkelbezug
// Kopie aus GAKA G5100 Programm, ausgefeilt!
double startDelay(int trigChannel, int *status) 
{
  double delay_absolut;
 
  // falls der Trig nicht T0 ist
  if(channel[trigChannel].trig != TNULL)                           
  {
    //ueberpruefe Zirkelbezug
    if(++(*status) > MAXCHAN) return ERROR;
    //ruft Prog rekursiv auf
    delay_absolut = startDelay(channel[trigChannel].trig, status);
    // Zirkelbezug erneut ueberpruefen
    if((*status) > MAXCHAN) return ERROR;                     
    else return ( delay_absolut + channel[trigChannel].delay * channel[trigChannel].sign);      
  }
  // falls Trig T0 ist: gibt den Startzeitpunkt des Kanals zur�ck
  else return (channel[trigChannel].delay * channel[trigChannel].sign); 
}

// createList: 
// Rueckgabe: Fehlernummer
// Sideeffekt: beschreibt globale Variable
int createList(void)
{
  unsigned int i;
  int err=0;
  sChgpatt tempItem;    // temp. Variable eines Listenelementes

  // Initialisiert die Hauptliste
  lFlanken = ListCreate(sizeof(sChgpatt));
  // Flags Register wird nicht gebraucht!
  tempItem.flags = 0;                
  
  //Erzeuge Listenelement fuer T0 Start
  tempItem.index = 0;
  tempItem.edge = RISING;
  tempItem.channel = TNULL;
  ListInsertItem(lFlanken, &tempItem, FRONT_OF_LIST);
  for(i=1; i<MAXCHAN; ++i){
    //wenn ein aktiver Kanal gefunden ist
    if(channel[i].act == EIN){
      // werden 2 Listenelemente angefuegt
      tempItem.index = channel[i].start;
      tempItem.edge = RISING;
      tempItem.channel = i;
      ListInsertItem(lFlanken, &tempItem, END_OF_LIST);
      tempItem.index = channel[i].stop;
      tempItem.edge = FALLING;
      tempItem.channel = i;
      ListInsertItem(lFlanken, &tempItem, END_OF_LIST);
    }// Ende if
  }// Ende for
  //Liste sortieren nach Index -> gleiche Index -> unbestimmte Reihenfolge
  //da qsort benutzt wird;
  //ruft eine selbst geschriebene Compare Fkt auf: ItemSort
  ListQuickSort(lFlanken, ItemSort);
  //Erzeuge Listenelement fuer T0 Stop aus letztem Element + 10Perioden
  //und passt globale Var. und GUI an (halt unerwartet hier)
  ListGetItem (lFlanken, &tempItem, END_OF_LIST);
  tempItem.index = tempItem.index + 2*MINDELAY_P;
  tempItem.edge = FALLING;
  tempItem.channel = TNULL;
  ListInsertItem(lFlanken, &tempItem, END_OF_LIST);
  channel[TNULL].stop = tempItem.index;
  channel[TNULL].lenght = (double)tempItem.index / MULT_FOR_P;
  SetCtrlVal(ph, PANEL_LEN_CH0, channel[TNULL].lenght);
  
  //Erzeugt das Flag Register in der Hauptliste
  setFlagReg(lFlanken);
#ifdef TESTEN //als Vergleich der Original- mit der indexkorrigierten Liste 
  //Schreibt Liste auf stdio zur Kontrolle
  listPrint(lFlanken);
#endif
  
  return err;
}
  

/******************************** Hilfsfunktionen  ***************************/

// writeConsole: schreibt den Zeitstempel und den Text in die Konsole.
// Kopie aus gaka G5100 Treiber, 2016
void writeConsole(char* text)
{
  time(&rawtime);                         // update time
  info = localtime( &rawtime );                 // Zeit in ein struct speichern
  strftime(timestamp,25,"\n[%d.%m.%Y %X]: ", info);       // f�r die Ausgabe formatieren
  SetCtrlVal (ph, PANEL_TEXTBOX_console, timestamp);    // und in die Konsole schreiben
  SetCtrlVal (ph, PANEL_TEXTBOX_console, text);     // und in die Konsole schreiben
}

// CompareFunktion fuers Sortieren aus NI Comm Example Code von JD_war_eagle
// Input: Zeiger auf 2 Elemente der zu sortierenden Liste
// Rueckgabe: +1 (1>2), -1 (1<2), 0 (1=2)
// Fehler in Quelle, musste > durch - ersetzen im Vergleich
int CVICALLBACK ItemSort(void *item1, void *item2)
{
  int result = 0;
  
  //Cast the items to the struct type
  sChgpatt* s1 = (sChgpatt*)item1;
  sChgpatt* s2 = (sChgpatt*)item2;
  
  if(s1->index == s2->index)
    result = 0;
  else if((s1->index - s2->index) > 0)
    result = 1;
  else
    result = -1;
  
  return result;
}

// Listet die Liste auf dem Std I/O Fenster
// Input: Liste
// benutzt die Struktur sChgpatt, daher nicht allgemeingueltig
void listPrint(ListType List)
{
  int itemCount = 0;
  sChgpatt *tempItem = NULL;
  
//If there are items in the list, print them
  if ((itemCount = ListNumItems(List)) > 0)
  {
    //Start the index at 1 because ListType indeces begin at 1
    for (int i = 1; i <= itemCount; i++)
    {
      if ((tempItem = ListGetPtrToItem (List, i))!= NULL)
      {
        //specifier unklar fuer __int64, %I64u koennte auch gehen
        printf ("Index= %jd, Edge= %d, Chan= %d, Flags = %u\n",
                tempItem->index, tempItem->edge, tempItem->channel,
                tempItem->flags);
        
      }
    }   
  }
  return;
}

//Erzeugt das Flag Register in der Hauptliste
//Input: die Hauptliste 
//Sideeffekt: Schreibt das Flags Register eines Listenelements 
//Achtung! Nicht unabh�ngig von seinen Nachbarelementen,als einziges Feld
void setFlagReg(ListType List)
{
  int itemCount=0;
  unsigned int mask=0, tempRegister=0;
  sChgpatt *tempItem = NULL;
  
  //If there are items in the list
  if ((itemCount = ListNumItems(List)) > 0)
  {
    //Start the index at 1 because ListType indeces begin at 1
    for (int i = 1; i <= itemCount; i++)
    {
      if ((tempItem = ListGetPtrToItem (List, i))!= NULL)
      {
        //Maske erstellen durch Schieberegister fuer Kanal des i-ten Elements
        mask = 1 << (tempItem->channel);
        if(tempItem->edge == RISING){
          //StartFlanke -> Kanal setzen durch bitweise OR mit 2^Kanal
          tempRegister = tempRegister | mask;
        }else{
          //StopFlanke -> Kanal zuruecksetzen durch bitweise AND mit not(mask)
          tempRegister = tempRegister & ~mask;
        }
        //setzt das Register des Listenelementes
        tempItem->flags = tempRegister;
        
      }//End if
    }//End for   
  }//End if
  return;
}

//Eliminiert Elemente mit gleichem Index in der Hauptliste
//Input: die Hauptliste 
//Return: Verschlankte Liste, nicht mehr global 
//        oder NULL falls Fehler -> Abfragen
ListType elimEqualIndex(ListType originalList)
{
  int itemCount=0;
 
  ListType newList;
  sChgpatt *tempItemN = NULL;  //aktuelles Element bei n
  sChgpatt *tempItemNm1 = NULL;//vorgaengiges Element bei n-1
  
  newList = ListCreate(sizeof(sChgpatt));
  //If there are items in the list
  if ((itemCount = ListNumItems(originalList)) > 0)
  {
    //Erstes Elemet der Liste, Listenzaehler starten mit 1
    if ((tempItemN = ListGetPtrToItem (originalList, 1))!= NULL){
      ; //tue nichts
    }else return NULL;
    //Start the index at 2 to zweitletztem Element
    for (int i = 2; i < itemCount; i++)
    {
      tempItemNm1 = tempItemN; //????  geht das  ????
      //Originallistenelement lesen
      if ((tempItemN = ListGetPtrToItem (originalList, i))!= NULL)
      {
        //falls die Index gleich sind -> gleiche Zeit
        if(tempItemN->index == tempItemNm1->index){
          ; //nichts tun, for-Schleife laeuft weiter
        }else{
          //falls ungleich, altes Element in neue Liste anhaengen
          ListInsertItem(newList, tempItemNm1, END_OF_LIST);
        }
       }else return NULL;//End if
    }//End for
    //Letztes Listenelement
    tempItemNm1 = tempItemN;
    if ((tempItemN = ListGetPtrToItem (originalList, END_OF_LIST))!= NULL){
      //falls die Index gleich sind -> gleiche Zeit, duerfte nie passieren
      if(tempItemN->index == tempItemNm1->index){
        ListInsertItem(newList, tempItemN, END_OF_LIST);
      }else{
        //falls ungleich, altes und aktuelles Element in neue Liste anhaengen
        ListInsertItem(newList, tempItemNm1, END_OF_LIST);
        ListInsertItem(newList, tempItemN, END_OF_LIST);
      }
    }else return NULL;
    
  }else return NULL;//End if
  
  return newList;
}

//schreibt die globale Vaiable in File
//Input: FileHandle
//Return: Fehlermeldung
int saveGlobalVar(FILE *fh)
{
  int i, err=0, errcnt=0, cnt=0;
  char buff[MAXBUF];
  //Kopfzeile, Kolonnenbeschriftung
  
  errcnt = fprintf(fh, "#Temp File "FILE_FOR_GLOBAL_VAR" for global Variable\n");
  if(errcnt<0) return ERR_WRITE_FILE;
  errcnt = fprintf(fh,
                   "#Num\t#Flag\t#Name\t#Trig\t#Sign\t#Delay\t#Lenght\t#Log\n"
                  );
  if(errcnt<0) return ERR_WRITE_FILE;
  for(i=0; i<MAXCHAN; i++){
    cnt = Fmt(buff,
              "%s<%i\t%i\t%s[]\t%i\t%i\t%f[p2]\t%f[p2]\t%i\n",
              i,
              channel[i].act,
              channel[i].name,
              channel[i].trig,
              channel[i].sign,
              channel[i].delay,
              channel[i].lenght,
              channel[i].logic);
    errcnt = fprintf(fh, buff);
    if(errcnt<0) return ERR_WRITE_FILE;
  }
  
  return err;
}

//liest die globale Vaiable aus Temporaer File
//Input: FileHandle
//Return: Fehlermeldung: 0->kein Fehler, -1 -> FileFehler
//Side Effekt: Viele, schreibt glob Variabel
//             und setzt die Felder des UIR entsprechend
int readFileToGlobalVar(FILE *fh)
{
  int i, j, err=0;
  int cnt=0;
  int chncnt=0; //nimmt aus der Datei den 1 Wert auf - unbenutzt
  //Puffer fuer 1 Zeile von fgets()
  char* line = malloc(sizeof(char) * MAXBUF);
  //Kopfzeile, Kolonnenbeschriftung  ignorieren
  //skip NUM_COMMENT_LINES lines
  for (i=0;i<NUM_COMMENT_LINES;i++) {
    //skip till the end of the line - woher bloss? DASP?
    fscanf(fh,"%*[^\n]");
    //go to new line 
    fscanf(fh,"%*1[\n]"); 
  }
  for(i=0; i<MAXCHAN; i++){
    //liest Zeile ein (bis \n)
    fgets(line, MAXBUF, fh);
    cnt = Scan(line,
               "%s>%d%d%s%d%d%f[p2]%f[p2]%d",
               &chncnt,
               &channel[i].act,
               channel[i].name,
               &channel[i].trig,
               &channel[i].sign,
               &channel[i].delay,
               &channel[i].lenght,
               &channel[i].logic);
    //wenn alle 8 Felder uebernommen wurden setzte UIR Controls
    //und bei aktiven Kanaelen mache sie sichtbar
    if(cnt == 8){
      SetCtrlVal(ph, ch[i][1], channel[i].name);
      SetCtrlVal(ph, ch[i][2], channel[i].trig);
      SetCtrlVal(ph, ch[i][3], channel[i].sign);
      SetCtrlVal(ph, ch[i][4], channel[i].delay);
      SetCtrlVal(ph, ch[i][5], channel[i].lenght);
      SetCtrlVal(ph, ch[i][6], channel[i].logic);
      if(channel[i].act == EIN){
        SetCtrlVal(ph, ch[i][0], channel[i].act);
        //alle Kanalparameter werden aktiviert ausser FLAG
        for(j=1; j<7; ++j){
          SetCtrlAttribute(ph, ch[i][j], ATTR_DIMMED, AUS);
        }//End for
      }//End if
    }else err = -1;
  }//End for
  
  return err;
}

/*
burstfile = fopen("T2jumpH_hasu.dat","r");
  if (burstfile==NULL) {                                      
    printf("error opening the burstfile");
  } else {
  //skip 10 lines
  for (i=0;i<10;i++) {
    fscanf(burstfile,"%*[^\n]"); //skip till the end of the line
    fscanf(burstfile,"%*1[\n]"); //go to new line
  }
  i = 0;
  ok = 1;
  while (ok == 1) {
    i++;
    fscanf(burstfile,"%d",&timing[i]);
    fscanf(burstfile,"%s",temp);
    flag[i]=strtol(temp,NULL,16);

    if (timing[i] == 1.0*i*mus) {
       ok = 0;
    } else{
    printf("timing: %d ",timing[i]);
    printf("flag: %s ",temp);
    printf("----> %d \n",flag[i]);

    fscanf(burstfile,"%*[^\n]"); //skip till the end of the line
    fscanf(burstfile,"%*1[\n]"); //go to new line
    }
  }
  }
 */
