/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 2020. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  PANEL                            1
#define  PANEL_DELAY_CH15                 2       /* control type: numeric, callback function: (none) */
#define  PANEL_LEN_CH15                   3       /* control type: numeric, callback function: (none) */
#define  PANEL_DELAY_CH14                 4       /* control type: numeric, callback function: (none) */
#define  PANEL_LOG_CH15                   5       /* control type: radioButton, callback function: (none) */
#define  PANEL_NAME_CH15                  6       /* control type: string, callback function: (none) */
#define  PANEL_LEN_CH14                   7       /* control type: numeric, callback function: (none) */
#define  PANEL_FLAG_CH15                  8       /* control type: radioButton, callback function: setChanProp */
#define  PANEL_DELAY_CH13                 9       /* control type: numeric, callback function: (none) */
#define  PANEL_LOG_CH14                   10      /* control type: radioButton, callback function: (none) */
#define  PANEL_SIGN_CH15                  11      /* control type: ring, callback function: (none) */
#define  PANEL_NAME_CH14                  12      /* control type: string, callback function: (none) */
#define  PANEL_LEN_CH13                   13      /* control type: numeric, callback function: (none) */
#define  PANEL_FLAG_CH14                  14      /* control type: radioButton, callback function: setChanProp */
#define  PANEL_DELAY_CH12                 15      /* control type: numeric, callback function: (none) */
#define  PANEL_LOG_CH13                   16      /* control type: radioButton, callback function: (none) */
#define  PANEL_TEXTMSG_32                 17      /* control type: textMsg, callback function: (none) */
#define  PANEL_SIGN_CH14                  18      /* control type: ring, callback function: (none) */
#define  PANEL_NAME_CH13                  19      /* control type: string, callback function: (none) */
#define  PANEL_LEN_CH12                   20      /* control type: numeric, callback function: (none) */
#define  PANEL_FLAG_CH13                  21      /* control type: radioButton, callback function: setChanProp */
#define  PANEL_DELAY_CH11                 22      /* control type: numeric, callback function: (none) */
#define  PANEL_LOG_CH12                   23      /* control type: radioButton, callback function: (none) */
#define  PANEL_TEXTMSG_30                 24      /* control type: textMsg, callback function: (none) */
#define  PANEL_SIGN_CH13                  25      /* control type: ring, callback function: (none) */
#define  PANEL_NAME_CH12                  26      /* control type: string, callback function: (none) */
#define  PANEL_LEN_CH11                   27      /* control type: numeric, callback function: (none) */
#define  PANEL_FLAG_CH12                  28      /* control type: radioButton, callback function: setChanProp */
#define  PANEL_DELAY_CH10                 29      /* control type: numeric, callback function: (none) */
#define  PANEL_LOG_CH11                   30      /* control type: radioButton, callback function: (none) */
#define  PANEL_TEXTMSG_28                 31      /* control type: textMsg, callback function: (none) */
#define  PANEL_SIGN_CH12                  32      /* control type: ring, callback function: (none) */
#define  PANEL_NAME_CH11                  33      /* control type: string, callback function: (none) */
#define  PANEL_LEN_CH10                   34      /* control type: numeric, callback function: (none) */
#define  PANEL_FLAG_CH11                  35      /* control type: radioButton, callback function: setChanProp */
#define  PANEL_DELAY_CH9                  36      /* control type: numeric, callback function: (none) */
#define  PANEL_LOG_CH10                   37      /* control type: radioButton, callback function: (none) */
#define  PANEL_TEXTMSG_26                 38      /* control type: textMsg, callback function: (none) */
#define  PANEL_SIGN_CH11                  39      /* control type: ring, callback function: (none) */
#define  PANEL_NAME_CH10                  40      /* control type: string, callback function: (none) */
#define  PANEL_LEN_CH9                    41      /* control type: numeric, callback function: (none) */
#define  PANEL_FLAG_CH10                  42      /* control type: radioButton, callback function: setChanProp */
#define  PANEL_DELAY_CH8                  43      /* control type: numeric, callback function: (none) */
#define  PANEL_LOG_CH9                    44      /* control type: radioButton, callback function: (none) */
#define  PANEL_TEXTMSG_24                 45      /* control type: textMsg, callback function: (none) */
#define  PANEL_SIGN_CH10                  46      /* control type: ring, callback function: (none) */
#define  PANEL_NAME_CH9                   47      /* control type: string, callback function: (none) */
#define  PANEL_LEN_CH8                    48      /* control type: numeric, callback function: (none) */
#define  PANEL_FLAG_CH9                   49      /* control type: radioButton, callback function: setChanProp */
#define  PANEL_DELAY_CH7                  50      /* control type: numeric, callback function: (none) */
#define  PANEL_LOG_CH8                    51      /* control type: radioButton, callback function: (none) */
#define  PANEL_TEXTMSG_22                 52      /* control type: textMsg, callback function: (none) */
#define  PANEL_SIGN_CH9                   53      /* control type: ring, callback function: (none) */
#define  PANEL_NAME_CH8                   54      /* control type: string, callback function: (none) */
#define  PANEL_LEN_CH7                    55      /* control type: numeric, callback function: (none) */
#define  PANEL_FLAG_CH8                   56      /* control type: radioButton, callback function: setChanProp */
#define  PANEL_DELAY_CH6                  57      /* control type: numeric, callback function: (none) */
#define  PANEL_LOG_CH7                    58      /* control type: radioButton, callback function: (none) */
#define  PANEL_TEXTMSG_20                 59      /* control type: textMsg, callback function: (none) */
#define  PANEL_SIGN_CH8                   60      /* control type: ring, callback function: (none) */
#define  PANEL_NAME_CH7                   61      /* control type: string, callback function: (none) */
#define  PANEL_LEN_CH6                    62      /* control type: numeric, callback function: (none) */
#define  PANEL_FLAG_CH7                   63      /* control type: radioButton, callback function: setChanProp */
#define  PANEL_DELAY_CH5                  64      /* control type: numeric, callback function: (none) */
#define  PANEL_LOG_CH6                    65      /* control type: radioButton, callback function: (none) */
#define  PANEL_TEXTMSG_18                 66      /* control type: textMsg, callback function: (none) */
#define  PANEL_SIGN_CH7                   67      /* control type: ring, callback function: (none) */
#define  PANEL_NAME_CH6                   68      /* control type: string, callback function: (none) */
#define  PANEL_LEN_CH5                    69      /* control type: numeric, callback function: (none) */
#define  PANEL_FLAG_CH6                   70      /* control type: radioButton, callback function: setChanProp */
#define  PANEL_DELAY_CH4                  71      /* control type: numeric, callback function: (none) */
#define  PANEL_LOG_CH5                    72      /* control type: radioButton, callback function: (none) */
#define  PANEL_TEXTMSG_16                 73      /* control type: textMsg, callback function: (none) */
#define  PANEL_SIGN_CH6                   74      /* control type: ring, callback function: (none) */
#define  PANEL_NAME_CH5                   75      /* control type: string, callback function: (none) */
#define  PANEL_LEN_CH4                    76      /* control type: numeric, callback function: (none) */
#define  PANEL_FLAG_CH5                   77      /* control type: radioButton, callback function: setChanProp */
#define  PANEL_DELAY_CH3                  78      /* control type: numeric, callback function: (none) */
#define  PANEL_LOG_CH4                    79      /* control type: radioButton, callback function: (none) */
#define  PANEL_TEXTMSG_14                 80      /* control type: textMsg, callback function: (none) */
#define  PANEL_SIGN_CH5                   81      /* control type: ring, callback function: (none) */
#define  PANEL_NAME_CH4                   82      /* control type: string, callback function: (none) */
#define  PANEL_LEN_CH3                    83      /* control type: numeric, callback function: (none) */
#define  PANEL_FLAG_CH4                   84      /* control type: radioButton, callback function: setChanProp */
#define  PANEL_DELAY_CH2                  85      /* control type: numeric, callback function: (none) */
#define  PANEL_LOG_CH3                    86      /* control type: radioButton, callback function: (none) */
#define  PANEL_TEXTMSG_12                 87      /* control type: textMsg, callback function: (none) */
#define  PANEL_SIGN_CH4                   88      /* control type: ring, callback function: (none) */
#define  PANEL_NAME_CH3                   89      /* control type: string, callback function: (none) */
#define  PANEL_LEN_CH2                    90      /* control type: numeric, callback function: (none) */
#define  PANEL_FLAG_CH3                   91      /* control type: radioButton, callback function: setChanProp */
#define  PANEL_DELAY_CH1                  92      /* control type: numeric, callback function: (none) */
#define  PANEL_LOG_CH2                    93      /* control type: radioButton, callback function: (none) */
#define  PANEL_TEXTMSG_10                 94      /* control type: textMsg, callback function: (none) */
#define  PANEL_SIGN_CH3                   95      /* control type: ring, callback function: (none) */
#define  PANEL_NAME_CH2                   96      /* control type: string, callback function: (none) */
#define  PANEL_LEN_CH1                    97      /* control type: numeric, callback function: (none) */
#define  PANEL_FLAG_CH2                   98      /* control type: radioButton, callback function: setChanProp */
#define  PANEL_DELAY_CH0                  99      /* control type: numeric, callback function: (none) */
#define  PANEL_LOG_CH0                    100     /* control type: radioButton, callback function: (none) */
#define  PANEL_LOG_CH1                    101     /* control type: radioButton, callback function: (none) */
#define  PANEL_TEXTMSG_8                  102     /* control type: textMsg, callback function: (none) */
#define  PANEL_SIGN_CH2                   103     /* control type: ring, callback function: (none) */
#define  PANEL_TEXTMSG_6                  104     /* control type: textMsg, callback function: (none) */
#define  PANEL_NAME_CH1                   105     /* control type: string, callback function: (none) */
#define  PANEL_SIGN_CH0                   106     /* control type: ring, callback function: (none) */
#define  PANEL_SIGN_CH1                   107     /* control type: ring, callback function: (none) */
#define  PANEL_LEN_CH0                    108     /* control type: numeric, callback function: (none) */
#define  PANEL_TRIG_CH15                  109     /* control type: ring, callback function: (none) */
#define  PANEL_TRIG_CH14                  110     /* control type: ring, callback function: (none) */
#define  PANEL_TRIG_CH13                  111     /* control type: ring, callback function: (none) */
#define  PANEL_TRIG_CH12                  112     /* control type: ring, callback function: (none) */
#define  PANEL_TRIG_CH11                  113     /* control type: ring, callback function: (none) */
#define  PANEL_TRIG_CH10                  114     /* control type: ring, callback function: (none) */
#define  PANEL_TRIG_CH9                   115     /* control type: ring, callback function: (none) */
#define  PANEL_TRIG_CH8                   116     /* control type: ring, callback function: (none) */
#define  PANEL_TRIG_CH7                   117     /* control type: ring, callback function: (none) */
#define  PANEL_TRIG_CH6                   118     /* control type: ring, callback function: (none) */
#define  PANEL_TRIG_CH5                   119     /* control type: ring, callback function: (none) */
#define  PANEL_TRIG_CH4                   120     /* control type: ring, callback function: (none) */
#define  PANEL_TRIG_CH3                   121     /* control type: ring, callback function: (none) */
#define  PANEL_TRIG_CH2                   122     /* control type: ring, callback function: (none) */
#define  PANEL_TRIG_CH1                   123     /* control type: ring, callback function: (none) */
#define  PANEL_FLAG_CH0                   124     /* control type: radioButton, callback function: (none) */
#define  PANEL_FLAG_CH1                   125     /* control type: radioButton, callback function: setChanProp */
#define  PANEL_NAME_CH0                   126     /* control type: string, callback function: (none) */
#define  PANEL_CREATE                     127     /* control type: command, callback function: makeFile */
#define  PANEL_CLOCK                      128     /* control type: numeric, callback function: calcCycle */
#define  PANEL_TEXTMSG_3                  129     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_33                 130     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_27                 131     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_31                 132     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_25                 133     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_29                 134     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_23                 135     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_21                 136     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_17                 137     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_19                 138     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_15                 139     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_13                 140     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_11                 141     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_9                  142     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_7                  143     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG                    144     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_2                  145     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_5                  146     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_4                  147     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_34                 148     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_47                 149     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_46                 150     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_45                 151     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_44                 152     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_43                 153     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_42                 154     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_40                 155     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_39                 156     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_38                 157     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_37                 158     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_36                 159     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_35                 160     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_48                 161     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_49                 162     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_41                 163     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_51                 164     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_50                 165     /* control type: textMsg, callback function: (none) */
#define  PANEL_HELP                       166     /* control type: command, callback function: help */
#define  PANEL_QUIT                       167     /* control type: command, callback function: QuitCallback */
#define  PANEL_NUMERIC_ERROR              168     /* control type: numeric, callback function: (none) */
#define  PANEL_SEND                       169     /* control type: command, callback function: progGeni */
#define  PANEL_OPEN                       170     /* control type: command, callback function: openFile */
#define  PANEL_TEXTBOX_console            171     /* control type: textBox, callback function: (none) */
#define  PANEL_TRIG_CH0                   172     /* control type: string, callback function: (none) */
#define  PANEL_TEXTMSG_cycle              173     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_52                 174     /* control type: textMsg, callback function: (none) */
#define  PANEL_TEXTMSG_53                 175     /* control type: textMsg, callback function: (none) */


     /* Control Arrays: */

          /* (no control arrays in the resource file) */


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */

int  CVICALLBACK calcCycle(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK help(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK makeFile(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK openFile(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK progGeni(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK QuitCallback(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK setChanProp(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
