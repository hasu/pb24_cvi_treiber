/*********************************
<pm_global.h>
Headerfile mit den globalen Daten 
*********************************/
#ifndef _PM_GLOBAL_H_
  #define _PM_GLOBAL_H_


#define MAXBUF 256  // Standart Buffergroesse
#define TNULL 0     // Entspricht dem Wert von T0 auf dem UIR RingCtrl
#define RISING 1    // Flankenrichtung
#define FALLING 0   // Flankenrichtung
#define FILE_FOR_GLOBAL_VAR "timelist.dat" // Name des Files fuer globale Var
#define NUM_COMMENT_LINES 2 // Anzahl Kommentarzeilen in TempFile


#ifndef EIN
#define EIN  1
#endif
#ifndef AUS
#define AUS 0
#endif

/******* Generatorabhaengige globale Variabeln *******/
// hier fuer den PulseBlaster PB24-USB von Spincore
#define MASTERCLK   100.0      // Master Clock: PB24-USB 100MHz
#define MAXTIME_P   4294966295 // 2^32-1001 als laengste Zeitdauer in Perioden
                               // entspricht 43 sec. at 100MHz
#define MINDELAY_P  5          // minimaler Delay in Perioden
#define MAXCHAN     16         // Anzahl Kanaele, auf 16 limitiert


#define MULT_FOR_P  100        // Faktor fuer Anzahl Perioden  
                               // Delay in us * 100 (auch MCLK in MHz)
#define PER_RES     1          // Anzahl Auflosungselemente in Timings
                               // der Master Periode: 10ns/0.01us

typedef struct pbinst_tag   // Abbild fuer Instruktionen des PB24
{
    __int64       index;  // Zeitpunkt der Schaltflanke in Perioden des MCLK
    int           channel;// zugehoeriger Kanal 0-15; 0 ist der T0
    int           edge;   // Richtung RISING=1 FALLING=0
    unsigned int  flags;  // 4bytes um die 24 Kanaele des PB24 abzubilden
} sChgpatt;              // steht fuer struct 'change pattern'

static ListType lFlanken = 0;  // steht fuer Liste Flanken

/******* allgemeine globale Variabeln *******/

struct timing
{
  char name[7];   // max 6 Zeichen, Bsp: bit15
  int act;        // 1 = Aktiv
  int logic;      // 0 = Normal, 1 = Invertiert
  int sign;       // Vorzeichen, +1 / -1
  int trig;       // Kanal der Triggerfunktion hat(1-15(Kanal1..15) oder 0(T0))
  double delay;   // Verzoegerung in us nach Trigger (any channel), positive
  double lenght;  // Pulsdauer in us, immer positiv
  __int64 start;      // Absolut bezueglich T0: Beginn in Perioden des MasterCLK
  __int64 stop;       // Absolut bezueglich T0: Ende in Perioden des MasterCLK
  
} channel[MAXCHAN];




/******* UIR bezogene globale Variabeln *******/

static int ph; //PanelHandle des Hauptpanels

// Versuch, die unzaehligen GUI Elemente als Tabelle abzubilden
// sollte programmatischen Zugriff auf ControlID vereinfachen
// im Moment mit 16 Kanaelen a 7 Eigenschaften
static int ch[16][7] = {
{PANEL_FLAG_CH0,PANEL_NAME_CH0,PANEL_TRIG_CH0,PANEL_SIGN_CH0,PANEL_DELAY_CH0,PANEL_LEN_CH0,PANEL_LOG_CH0},  
{PANEL_FLAG_CH1,PANEL_NAME_CH1,PANEL_TRIG_CH1,PANEL_SIGN_CH1,PANEL_DELAY_CH1,PANEL_LEN_CH1,PANEL_LOG_CH1},
{PANEL_FLAG_CH2,PANEL_NAME_CH2,PANEL_TRIG_CH2,PANEL_SIGN_CH2,PANEL_DELAY_CH2,PANEL_LEN_CH2,PANEL_LOG_CH2},
{PANEL_FLAG_CH3,PANEL_NAME_CH3,PANEL_TRIG_CH3,PANEL_SIGN_CH3,PANEL_DELAY_CH3,PANEL_LEN_CH3,PANEL_LOG_CH3},
{PANEL_FLAG_CH4,PANEL_NAME_CH4,PANEL_TRIG_CH4,PANEL_SIGN_CH4,PANEL_DELAY_CH4,PANEL_LEN_CH4,PANEL_LOG_CH4},
{PANEL_FLAG_CH5,PANEL_NAME_CH5,PANEL_TRIG_CH5,PANEL_SIGN_CH5,PANEL_DELAY_CH5,PANEL_LEN_CH5,PANEL_LOG_CH5},
{PANEL_FLAG_CH6,PANEL_NAME_CH6,PANEL_TRIG_CH6,PANEL_SIGN_CH6,PANEL_DELAY_CH6,PANEL_LEN_CH6,PANEL_LOG_CH6},
{PANEL_FLAG_CH7,PANEL_NAME_CH7,PANEL_TRIG_CH7,PANEL_SIGN_CH7,PANEL_DELAY_CH7,PANEL_LEN_CH7,PANEL_LOG_CH7},
{PANEL_FLAG_CH8,PANEL_NAME_CH8,PANEL_TRIG_CH8,PANEL_SIGN_CH8,PANEL_DELAY_CH8,PANEL_LEN_CH8,PANEL_LOG_CH8},
{PANEL_FLAG_CH9,PANEL_NAME_CH9,PANEL_TRIG_CH9,PANEL_SIGN_CH9,PANEL_DELAY_CH9,PANEL_LEN_CH9,PANEL_LOG_CH9},
{PANEL_FLAG_CH10,PANEL_NAME_CH10,PANEL_TRIG_CH10,PANEL_SIGN_CH10,PANEL_DELAY_CH10,PANEL_LEN_CH10,PANEL_LOG_CH10},
{PANEL_FLAG_CH11,PANEL_NAME_CH11,PANEL_TRIG_CH11,PANEL_SIGN_CH11,PANEL_DELAY_CH11,PANEL_LEN_CH11,PANEL_LOG_CH11},
{PANEL_FLAG_CH12,PANEL_NAME_CH12,PANEL_TRIG_CH12,PANEL_SIGN_CH12,PANEL_DELAY_CH12,PANEL_LEN_CH12,PANEL_LOG_CH12},
{PANEL_FLAG_CH13,PANEL_NAME_CH13,PANEL_TRIG_CH13,PANEL_SIGN_CH13,PANEL_DELAY_CH13,PANEL_LEN_CH13,PANEL_LOG_CH13},
{PANEL_FLAG_CH14,PANEL_NAME_CH14,PANEL_TRIG_CH14,PANEL_SIGN_CH14,PANEL_DELAY_CH14,PANEL_LEN_CH14,PANEL_LOG_CH14},
{PANEL_FLAG_CH15,PANEL_NAME_CH15,PANEL_TRIG_CH15,PANEL_SIGN_CH15,PANEL_DELAY_CH15,PANEL_LEN_CH15,PANEL_LOG_CH15}};


/********************* Berechnungsfunktions Prototypen ***********************/
int fillData(void);    // erzeugt Timings in globaler Variabeln 
int detAbsStart(void); // bestimmt absolute Startzeiten bez. T0
double startDelay(int trigChannel, int *status); // Rekursive Fkt, bestimmt
                                                 // Startzeitpunkte absolut
int createList(void);  // erzeugt Liste mit Elementen bestehend aus 
                       // Flankenzeitpunkt und Flags (Kanaele,die aendern)
void setFlagReg(ListType List); //Erzeugt das Flag Register in der Hauptliste
ListType elimEqualIndex(ListType originalList); //Bereinigt gleiche Index (Zeit)

/********************* Hilfs-Funktions Prototypen ****************************/
void writeConsole(char* text);  // writes a text to the UI-console
int CVICALLBACK ItemSort(void *item1, void *item2); // Compare Fkt fuer ListQuickSort
void listPrint(ListType list); // Schreibt Liste auf STD I/O
int saveGlobalVar(FILE *fh); // Schreibt die globale Vaiable in File
int readFileToGlobalVar(FILE *fh); //Liest aus TemporaerFile & in globale Var. 

time_t rawtime;     // Rohzeit
struct tm *info;    // Zeit Struct
char timestamp[25]; // Zeitstempel im Format: "dd.mm.yy hh:mm:ss"


#endif /* _PM_GLOBAL_H_ */
