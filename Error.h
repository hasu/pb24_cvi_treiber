/****************************************************/
/*  Projekt:  G5100 Pattern Generator         */
/*  Datei:    Error.h                 */
/*  Datum:    08.06.2016                */
/*  Last edit:  07.11.2016              */ 
/*  Ersteller:  Gabriel K�ppeli             */
/*  Funktion:   In dieser Datei sind die Fehler   */
/*        definiert.              */
/****************************************************/

// P:\elektronik\_Lehrlinge\Daten_GabrielKaeppeli_15-17\Projekte\G5100_AWG_Picotest\Software\VISA_UI_direct

#define NO_ERROR        0   // Kein Fehler
#define SUCCESS         0   // Erfolgreich
#define ERROR         -1    // Fehler

#define ERR_TIMING        1000  // Zeitfehler:  die letzten zwei Ziffer stehen f�r den betroffenen Kanal (0-15)
#define ERR_MINIMAL_TIME_STEP 1100  // Zeitschrit zu klein (kein Vielfaches einer Periode)
#define ERR_MINIMAL_TIME    1200  // Triggerzeitpunkt kleiner T0
#define ERR_MAXIMAL_TIME    1300  // Maximale Zeit �berschritten (50Mhz -> 5.24288 ms)
#define ERR_CIRCULAR_REFERENCE  1400  // Zirkelbezug

#define ERR_FILE        2000    // Dateifehler:
#define ERR_WRITE_FILE  2001    // Fehler beim Schreiben eines Files
#define ERR_FILEPATH_NOT_FOUND  2002    // Der Dateipfad konnt nicht gefunden werden.
#define ERR_NO_FILE_SELECTED  2003  // Es wurde keine Datei ausgew�hlt

#define ERR_LIST_PREPARE 3000 //Allerweltsfehler beim Listenerzeugen

#define ERR_PULSGENI_INIT 4000 //bei Initialisierung was schiefgelaufen

